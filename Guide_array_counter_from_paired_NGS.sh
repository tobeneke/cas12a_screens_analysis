#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------------------------

FORWARD=100XLib_R1_001.fastq.gz
REVERSE=100XLib_R2_001.fastq.gz

#--------------------------------------------------------------------------------------------------------------------------------------------------
#DR
REFERENCE_GENOMEDR=DR_single_line.fasta
NAMEDR=DR-Mini-pool-plasmid-test

mkdir DR
cp $REFERENCE_GENOMEDR DR
cd DR

echo "You are uploading <"$REFERENCE_GENOMEDR"> as a reference genome."


echo "You are uploading <"$FORWARD"> as forward read."


echo "You are uploading <"$REVERSE"> as reverse read."


echo "You are uploading <"$NAMEDR"> as name."

echo "Thanks for your input. Your genome will be now processed! This might take a while...."

#bwa aligner
echo "running bwa alignment"
bwa index $REFERENCE_GENOMEDR
bwa mem $REFERENCE_GENOMEDR ../$FORWARD ../$REVERSE > $NAMEDR-aln-pe.sam

#sorting
echo "sorting and indexing"
samtools view -S -b $NAMEDR-aln-pe.sam > $NAMEDR-aln-pe.bam
samtools sort $NAMEDR-aln-pe.bam -o $NAMEDR-aln-pe.s.bam

#insert size filter, excluding reads with indels and indexing
samtools view -h $NAMEDR-aln-pe.s.bam | \
  awk '$1 ~ "^@" || $6 !~ "I|D"' | \
  samtools view -b - > $NAMEDR-aln-pe.s0.bam
samtools view -h $NAMEDR-aln-pe.s0.bam | \
  awk 'substr($0,1,1)=="@" || ($9>= 87 && $9<=89) || ($9<=-87 && $9>=-89)' | \
  samtools view -b > $NAMEDR-aln-pe.sorted.bam
samtools index $NAMEDR-aln-pe.sorted.bam

#get statistics of alignment
echo "generating stat alignments"

mkdir no-qual-plot
samtools stats $NAMEDR-aln-pe.s.bam > $NAMEDR-aln-pe.stats.no-qual.txt
cd no-qual-plot
plot-bamstats -p plot ../$NAMEDR-aln-pe.stats.no-qual.txt
cd ..

mkdir qual-plot
samtools stats $NAMEDR-aln-pe.sorted.bam > $NAMEDR-aln-pe.stats.qual.txt
cd qual-plot
plot-bamstats -p plot ../$NAMEDR-aln-pe.stats.qual.txt
cd ..

#count reads per amplicon
samtools idxstats $NAMEDR-aln-pe.sorted.bam > $NAMEDR-aln-pe.sorted.bam.amplicon.counts.txt

#get unmapped reads
samtools view -b -f 4 $NAMEDR-aln-pe.sorted.bam | samtools bam2fq | seqtk seq -A > unmapped.fa
sed '/>/d' ./unmapped.fa | sort | uniq -c | sort -k1,1nr -k2,2 > count.sort.unmapped.txt

echo "Done DR"
cd ..

#--------------------------------------------------------------------------------------------------------------------------------------------------
#1x
REFERENCE_GENOMEx1=1xDR_single_line.fasta
NAMEx1=1x-Mini-pool-plasmid-test

mkdir 1x
cp $REFERENCE_GENOMEx1 1x
cd 1x

echo "You are uploading <"$REFERENCE_GENOMEx1"> as a reference genome."


echo "You are uploading <"$FORWARD"> as forward read."


echo "You are uploading <"$REVERSE"> as reverse read."


echo "You are uploading <"$NAMEx1"> as name."

echo "Thanks for your input. Your genome will be now processed! This might take a while...."

#bwa aligner
echo "running bwa alignment"
bwa index $REFERENCE_GENOMEx1
bwa mem $REFERENCE_GENOMEx1 ../$FORWARD ../$REVERSE > $NAMEx1-aln-pe.sam

#sorting
echo "sorting and indexing"
samtools view -S -b $NAMEx1-aln-pe.sam > $NAMEx1-aln-pe.bam
samtools sort $NAMEx1-aln-pe.bam -o $NAMEx1-aln-pe.s.bam

#insert size filter, excluding reads with indels and indexing
samtools view -h $NAMEx1-aln-pe.s.bam | \
  awk '$1 ~ "^@" || $6 !~ "I|D"' | \
  samtools view -b - > $NAMEx1-aln-pe.s0.bam
samtools view -h $NAMEx1-aln-pe.s0.bam | \
  awk 'substr($0,1,1)=="@" || ($9>= 127 && $9<=129) || ($9<=-127 && $9>=-129)' | \
  samtools view -b > $NAMEx1-aln-pe.sorted.bam
samtools index $NAMEx1-aln-pe.sorted.bam

#get statistics of alignment
echo "generating stat alignments"

mkdir no-qual-plot
samtools stats $NAMEx1-aln-pe.s.bam > $NAMEx1-aln-pe.stats.no-qual.txt
cd no-qual-plot
plot-bamstats -p plot ../$NAMEx1-aln-pe.stats.no-qual.txt
cd ..

mkdir qual-plot
samtools stats $NAMEx1-aln-pe.sorted.bam > $NAMEx1-aln-pe.stats.qual.txt
cd qual-plot
plot-bamstats -p plot ../$NAMEx1-aln-pe.stats.qual.txt
cd ..

#count reads per amplicon
samtools idxstats $NAMEx1-aln-pe.sorted.bam > $NAMEx1-aln-pe.sorted.bam.amplicon.counts.txt

#get unmapped reads
samtools view -b -f 4 $NAMEx1-aln-pe.sorted.bam | samtools bam2fq | seqtk seq -A > unmapped.fa
sed '/>/d' ./unmapped.fa | sort | uniq -c | sort -k1,1nr -k2,2 > count.sort.unmapped.txt

echo "Done x1"
cd ..

#--------------------------------------------------------------------------------------------------------------------------------------------------

#2x
REFERENCE_GENOMEx2=2xDR_single_line.fasta
NAMEx2=2x-Mini-pool-plasmid-test

mkdir 2x
cp $REFERENCE_GENOMEx2 2x
cd 2x

echo "You are uploading <"$REFERENCE_GENOMEx2"> as a reference genome."


echo "You are uploading <"$FORWARD"> as forward read."


echo "You are uploading <"$REVERSE"> as reverse read."


echo "You are uploading <"$NAMEx2"> as name."

echo "Thanks for your input. Your genome will be now processed! This might take a while...."

#bwa aligner
echo "running bwa alignment"
bwa index $REFERENCE_GENOMEx2
bwa mem $REFERENCE_GENOMEx2 ../$FORWARD ../$REVERSE > $NAMEx2-aln-pe.sam

#sorting
echo "sorting and indexing"
samtools view -S -b $NAMEx2-aln-pe.sam > $NAMEx2-aln-pe.bam
samtools sort $NAMEx2-aln-pe.bam -o $NAMEx2-aln-pe.s.bam

#insert size filter, excluding reads with indels and indexing
samtools view -h $NAMEx2-aln-pe.s.bam | \
  awk '$1 ~ "^@" || $6 !~ "I|D"' | \
  samtools view -b - > $NAMEx2-aln-pe.s0.bam
samtools view -h $NAMEx2-aln-pe.s0.bam | \
  awk 'substr($0,1,1)=="@" || ($9>= 167 && $9<=169) || ($9<=-167 && $9>=-169)' | \
  samtools view -b > $NAMEx2-aln-pe.sorted.bam
samtools index $NAMEx2-aln-pe.sorted.bam

#get statistics of alignment
echo "generating stat alignments"

mkdir no-qual-plot
samtools stats $NAMEx2-aln-pe.s.bam > $NAMEx2-aln-pe.stats.no-qual.txt
cd no-qual-plot
plot-bamstats -p plot ../$NAMEx2-aln-pe.stats.no-qual.txt
cd ..

mkdir qual-plot
samtools stats $NAMEx2-aln-pe.sorted.bam > $NAMEx2-aln-pe.stats.qual.txt
cd qual-plot
plot-bamstats -p plot ../$NAMEx2-aln-pe.stats.qual.txt
cd ..

#count reads per amplicon
samtools idxstats $NAMEx2-aln-pe.sorted.bam > $NAMEx2-aln-pe.sorted.bam.amplicon.counts.txt

#get unmapped reads
samtools view -b -f 4 $NAMEx2-aln-pe.sorted.bam | samtools bam2fq | seqtk seq -A > unmapped.fa
sed '/>/d' ./unmapped.fa | sort | uniq -c | sort -k1,1nr -k2,2 > count.sort.unmapped.txt

echo "Done x2"
cd ..

#--------------------------------------------------------------------------------------------------------------------------------------------------
#3x
REFERENCE_GENOMEx3=3xDR_single_line.fasta
NAMEx3=3x-Mini-pool-plasmid-test

mkdir 3x
cp $REFERENCE_GENOMEx3 3x
cd 3x

echo "You are uploading <"$REFERENCE_GENOMEx3"> as a reference genome."


echo "You are uploading <"$FORWARD"> as forward read."


echo "You are uploading <"$REVERSE"> as reverse read."


echo "You are uploading <"$NAMEx3"> as name."

echo "Thanks for your input. Your genome will be now processed! This might take a while...."

#bwa aligner
echo "running bwa alignment"
bwa index $REFERENCE_GENOMEx3
bwa mem $REFERENCE_GENOMEx3 ../$FORWARD ../$REVERSE > $NAMEx3-aln-pe.sam

#sorting
echo "sorting and indexing"
samtools view -S -b $NAMEx3-aln-pe.sam > $NAMEx3-aln-pe.bam
samtools sort $NAMEx3-aln-pe.bam -o $NAMEx3-aln-pe.s.bam

#insert size filter, excluding reads with indels and indexing
samtools view -h $NAMEx3-aln-pe.s.bam | \
  awk '$1 ~ "^@" || $6 !~ "I|D"' | \
  samtools view -b - > $NAMEx3-aln-pe.s0.bam
samtools view -h $NAMEx3-aln-pe.s0.bam | \
  awk 'substr($0,1,1)=="@" || ($9>= 207 && $9<=209) || ($9<=-207 && $9>=-209)' | \
  samtools view -b > $NAMEx3-aln-pe.sorted.bam
samtools index $NAMEx3-aln-pe.sorted.bam

#get statistics of alignment
echo "generating stat alignments"

mkdir no-qual-plot
samtools stats $NAMEx3-aln-pe.s.bam > $NAMEx3-aln-pe.stats.no-qual.txt
cd no-qual-plot
plot-bamstats -p plot ../$NAMEx3-aln-pe.stats.no-qual.txt
cd ..

mkdir qual-plot
samtools stats $NAMEx3-aln-pe.sorted.bam > $NAMEx3-aln-pe.stats.qual.txt
cd qual-plot
plot-bamstats -p plot ../$NAMEx3-aln-pe.stats.qual.txt
cd ..

#count reads per amplicon
samtools idxstats $NAMEx3-aln-pe.sorted.bam > $NAMEx3-aln-pe.sorted.bam.amplicon.counts.txt

#get unmapped reads
samtools view -b -f 4 $NAMEx3-aln-pe.sorted.bam | samtools bam2fq | seqtk seq -A > unmapped.fa
sed '/>/d' ./unmapped.fa | sort | uniq -c | sort -k1,1nr -k2,2 > count.sort.unmapped.txt

echo "Done x3"
cd ..

#--------------------------------------------------------------------------------------------------------------------------------------------------
#4x
REFERENCE_GENOMEx4=4xDR_single_line.fasta
NAMEx4=4x-Mini-pool-plasmid-test

mkdir 4x
cp $REFERENCE_GENOMEx4 4x
cd 4x

echo "You are uploading <"$REFERENCE_GENOMEx4"> as a reference genome."


echo "You are uploading <"$FORWARD"> as forward read."


echo "You are uploading <"$REVERSE"> as reverse read."


echo "You are uploading <"$NAMEx4"> as name."

echo "Thanks for your input. Your genome will be now processed! This might take a while...."

#bwa aligner
echo "running bwa alignment"
bwa index $REFERENCE_GENOMEx4
bwa mem $REFERENCE_GENOMEx4 ../$FORWARD ../$REVERSE > $NAMEx4-aln-pe.sam

#sorting
echo "sorting and indexing"
samtools view -S -b $NAMEx4-aln-pe.sam > $NAMEx4-aln-pe.bam
samtools sort $NAMEx4-aln-pe.bam -o $NAMEx4-aln-pe.s.bam

#insert size filter, excluding reads with indels and indexing
samtools view -h $NAMEx4-aln-pe.s.bam | \
  awk '$1 ~ "^@" || $6 !~ "I|D"' | \
  samtools view -b - > $NAMEx4-aln-pe.s0.bam
samtools view -h $NAMEx4-aln-pe.s0.bam | \
  awk 'substr($0,1,1)=="@" || ($9>= 247 && $9<=249) || ($9<=-247 && $9>=-249)' | \
  samtools view -b > $NAMEx4-aln-pe.sorted.bam
samtools index $NAMEx4-aln-pe.sorted.bam

#get statistics of alignment
echo "generating stat alignments"

mkdir no-qual-plot
samtools stats $NAMEx4-aln-pe.s.bam > $NAMEx4-aln-pe.stats.no-qual.txt
cd no-qual-plot
plot-bamstats -p plot ../$NAMEx4-aln-pe.stats.no-qual.txt
cd ..

mkdir qual-plot
samtools stats $NAMEx4-aln-pe.sorted.bam > $NAMEx4-aln-pe.stats.qual.txt
cd qual-plot
plot-bamstats -p plot ../$NAMEx4-aln-pe.stats.qual.txt
cd ..

#count reads per amplicon
samtools idxstats $NAMEx4-aln-pe.sorted.bam > $NAMEx4-aln-pe.sorted.bam.amplicon.counts.txt

#get unmapped reads
samtools view -b -f 4 $NAMEx4-aln-pe.sorted.bam | samtools bam2fq | seqtk seq -A > unmapped.fa
sed '/>/d' ./unmapped.fa | sort | uniq -c | sort -k1,1nr -k2,2 > count.sort.unmapped.txt

echo "Done x4"
cd ..
#--------------------------------------------------------------------------------------------------------------------------------------------------

echo "all done"
